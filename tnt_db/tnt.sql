-- -------------------------------------------------------------
-- TablePlus 4.5.2(402)
--
-- https://tableplus.com/
--
-- Database: tnt_db
-- Generation Time: 2022-01-13 10:27:45.7370
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `gorp_migrations`;
CREATE TABLE `gorp_migrations` (
  `id` varchar(255) NOT NULL,
  `applied_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS `tbl_company_tasks`;
CREATE TABLE `tbl_company_tasks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` text,
  `company_id` int NOT NULL,
  `project_id` int NOT NULL,
  `is_duplicate_task_for_selected_employee` tinyint(1) NOT NULL,
  `is_no_due_date` tinyint(1) NOT NULL,
  `is_sent_email_to_assigned_employee` tinyint(1) NOT NULL,
  `due_on` timestamp NULL DEFAULT NULL,
  `description` text,
  `status` int NOT NULL,
  `created_by` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_archive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_companyId` (`company_id`),
  KEY `IDX_status` (`status`),
  KEY `IDX_projectId` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_company_tasks_assignee`;
CREATE TABLE `tbl_company_tasks_assignee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_task_id` int NOT NULL,
  `project_id` int NOT NULL,
  `company_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_companyTaskId` (`company_task_id`),
  KEY `IDX_companyId` (`company_id`),
  KEY `IDX_userId` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_company_tasks_attachments`;
CREATE TABLE `tbl_company_tasks_attachments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_task_id` int NOT NULL,
  `company_id` int NOT NULL,
  `url_file` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_companyTaskId` (`company_task_id`),
  KEY `IDX_companyId` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_company_tasks_comments`;
CREATE TABLE `tbl_company_tasks_comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_task_id` int NOT NULL,
  `company_id` int NOT NULL,
  `user_id` int NOT NULL,
  `comment` text,
  `url_file` text,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_change_status_task` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_companyTaskId` (`company_task_id`),
  KEY `IDX_companyId` (`company_id`),
  KEY `IDX_userId` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_job`;
CREATE TABLE `tbl_job` (
  `id` int NOT NULL AUTO_INCREMENT,
  `company_id` int NOT NULL,
  `job_id` varchar(50) NOT NULL,
  `job` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `level` int NOT NULL DEFAULT '1',
  `branch_id` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_projects`;
CREATE TABLE `tbl_projects` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `company_id` int NOT NULL,
  `created_by` int NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:Active ,2:Archive',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_companyId` (`company_id`),
  KEY `IDX_createdBy` (`created_by`),
  KEY `IDX_status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_projects_assignee`;
CREATE TABLE `tbl_projects_assignee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `project_id` int NOT NULL,
  `company_id` int NOT NULL,
  `user_id` int NOT NULL,
  `role` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_id` (`id`),
  KEY `IDX_projectId` (`project_id`),
  KEY `IDX_companyId` (`company_id`),
  KEY `IDX_userId` (`user_id`),
  KEY `IDX_role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_tasks`;
CREATE TABLE `tbl_tasks` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fk_ref` int NOT NULL,
  `company_id` int NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `assigned_to` int NOT NULL COMMENT 'fk pointing to tbl_user pk',
  `assigned_from` int NOT NULL COMMENT 'fk pointing to tbl_user pk',
  `deadline` datetime DEFAULT NULL,
  `status_employee_tasks` tinyint NOT NULL DEFAULT '0' COMMENT '0:unfinish,1:done confirmation from employee',
  `status_tasks` tinyint NOT NULL DEFAULT '0' COMMENT '0:unchecked,1:checked, confirmation from hr admin',
  `type_task` tinyint NOT NULL DEFAULT '0' COMMENT '0 : others ; 1 : onboarding',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `delete_flag` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_tasks_comment`;
CREATE TABLE `tbl_tasks_comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_commented` int NOT NULL,
  `comment` text NOT NULL,
  `task_id` int NOT NULL,
  `company_id` int NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `tbl_time_sheet`;
CREATE TABLE `tbl_time_sheet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `task_id` int DEFAULT NULL,
  `assignee_id` int NOT NULL,
  `company_id` int NOT NULL,
  `activity` text,
  `status` int DEFAULT '0',
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `starttime_type` int DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `IDX_taskId` (`task_id`),
  KEY `IDX_assigneeId` (`assignee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `id_employee` varchar(200) NOT NULL,
  `citizen_id` varchar(200) NOT NULL,
  `type_citizen_id` tinyint NOT NULL DEFAULT '0' COMMENT '1: KTP; 2: Passport;',
  `expired_date_citizen_id` date DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `title` int NOT NULL COMMENT 'pointing to tbl_title PK',
  `job_id` int NOT NULL COMMENT 'pointing to tbl_job Pk',
  `organization_id` int NOT NULL COMMENT 'pointing to tbl_organization Pk',
  `gender` tinyint NOT NULL COMMENT '1:Male, 2:Female',
  `birth_date` date DEFAULT NULL,
  `birth_place` varchar(50) DEFAULT NULL,
  `address` text NOT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `religion` tinyint unsigned DEFAULT NULL COMMENT '1 : Catholic; 2 : Islam; 3 : Christian; 4 : Buddha; 5 : Hindu; 6 : Confucius',
  `marital_status` tinyint NOT NULL COMMENT '1:single, 2: married, 3: widow, 4:widower',
  `blood_type` tinyint DEFAULT NULL COMMENT '1:A; 2:B; 3:AB; 4:O',
  `mobile_phone` varchar(20) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(100) DEFAULT NULL,
  `salary` int NOT NULL,
  `tax_config` tinyint NOT NULL DEFAULT '1' COMMENT '1: gross, 2: netto',
  `salary_config` tinyint NOT NULL COMMENT '1: taxable; 2: nontaxable',
  `type_salary` tinyint NOT NULL DEFAULT '1' COMMENT '1: Monthly; 2: Daily',
  `bpjstk_config` tinyint NOT NULL DEFAULT '3' COMMENT '1 : By Company; 2 : By Employee; 3 : Default',
  `bpjsk_config` int NOT NULL DEFAULT '3' COMMENT '1 : By Company; 2 : By Employee',
  `jp_config` int NOT NULL DEFAULT '3' COMMENT '1 : By Company; 2 : By Employee',
  `tax_status` tinyint NOT NULL COMMENT '1:T0,2:T1,3:T2,4:T3,5:K0,6:K1,7:K2,8:K3',
  `jkk_status` tinyint NOT NULL,
  `bpjsk_family_child` int NOT NULL DEFAULT '0',
  `bank_name` tinyint DEFAULT NULL COMMENT '1:BCA,2:Mandiri,3:BRI,4:CIMB,5:Commonwealth',
  `bank_account` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(100) DEFAULT NULL,
  `npwp` varchar(200) NOT NULL,
  `bpjstk` varchar(200) NOT NULL,
  `bpjsk` varchar(200) NOT NULL,
  `status` tinyint NOT NULL DEFAULT '0' COMMENT '0: active user 1: deleted user/inactive',
  `status_employee` tinyint NOT NULL COMMENT '1:full_time ,2: part_time,3:internship,4:temporary',
  `join_date` date NOT NULL,
  `working_date` date DEFAULT NULL,
  `end_contract_date` date NOT NULL,
  `end_employee_date` date NOT NULL,
  `end_probation_date` date NOT NULL,
  `time_off_policies_id` int NOT NULL COMMENT 'fk pointing to pk tbl_time_off_policies',
  `role` int NOT NULL DEFAULT '1' COMMENT '1:super admin , 2:employee, 3: admin, other refer to tbl_access_role',
  `last_login` date DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pattern` int DEFAULT NULL,
  `token` text,
  `onboarding_id` int DEFAULT NULL,
  `overtime_component_standard_id` int DEFAULT NULL,
  `overtime_component_dayoff_id` int DEFAULT NULL,
  `branch_id` int NOT NULL,
  `prorate_type` int NOT NULL,
  `custom_number` int NOT NULL,
  `overtime_status` int NOT NULL DEFAULT '1' COMMENT '0 : no, 1 : yes',
  `employee_tax_status` int NOT NULL DEFAULT '0' COMMENT '0 : Pegawai Tetap; 1 : Pegawai Tidak Tetap; 2 : Expatriate; 3 : Bukan Pegawai yang bersifat berkesinambungan',
  `current_address` text NOT NULL,
  `jkk_config` int NOT NULL DEFAULT '0',
  `npwp_date` date DEFAULT NULL,
  `overtime_component_national_holiday_id` int NOT NULL,
  `expatriatedn_date` date DEFAULT NULL,
  `currency_id` text NOT NULL,
  `beginning_netto` double NOT NULL,
  `pph21_paid` double NOT NULL,
  `rehire_id` int NOT NULL,
  `nationality_code` text NOT NULL,
  `bpjsk_date` date DEFAULT NULL,
  `jp_date` date DEFAULT NULL,
  `bpjs_date` date DEFAULT NULL,
  `onboarding` int NOT NULL,
  `arrival_time` time NOT NULL,
  `payroll_payment_schedule_id` varchar(100) NOT NULL,
  `auto_login_token` text NOT NULL,
  `class_id` int DEFAULT NULL,
  `grade_id` int DEFAULT NULL,
  `override_holiday` int NOT NULL,
  `npp` int NOT NULL DEFAULT '0',
  `bank_account_status` int NOT NULL COMMENT '0 : not verified, 1: verified , 2 : wrong account',
  `bak_custom_password_payslip` text,
  `custom_password_payslip` text,
  `sso_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sso_id` (`sso_id`),
  KEY `company_id` (`company_id`),
  KEY `order_index` (`organization_id`,`id_employee`),
  KEY `email` (`email`),
  KEY `rehire_id` (`rehire_id`),
  KEY `IDX_joinDate_companyId` (`join_date`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;