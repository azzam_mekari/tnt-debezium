# Usage
```shell
# Start the application
docker-compose -f docker-compose.yaml up

# Get all registered connectors
curl -i -X GET http://localhost:8083/connectors/

# Start Talenta User Mysqlconnector
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @tnt-connector.json

# Start TNT User Sink
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @user-sink.json

# Start TNT Job Sink
curl -i -X POST -H "Accept:application/json" -H  "Content-Type:application/json" http://localhost:8083/connectors/ -d @job-sink.json

# Delete connectors

curl -i -X DELETE http://localhost:8083/connectors/{connector_name}

curl -i -X DELETE http://localhost:8083/connectors/tnt-connector
curl -i -X DELETE http://localhost:8083/connectors/user-sink
curl -i -X DELETE http://localhost:8083/connectors/job-sink
```